#!/usr/bin/env ruby

# Evita ejecutar el script si no se está en la raíz del repo
if File.directory?('scripts')
  Dir.chdir('scripts')
else
  puts "Necesitas estar en la raíz de este repositorio."
end

# Las 'parts' son pedazos a incrustar en los 'wholes'
parts = Dir.glob('static/part.*')
wholes = Dir.glob('static/whole.*')
out = '../src/static/templates'

# Las parts se convierten a un objeto
# que tiene su nombre de archivo y contenido
parts.map! do | path |
  {
    name: File.basename(path).strip,
    content: File.read(path)
  }
end

# Los wholes incrustan sus parts para después
# guardarse como template
wholes.each do | path | 
  name = File.basename(path).sub('whole.', 'template.')
  content = File.read(path).strip
  parts.each do | part |
    content.gsub!("##{part[:name]}#", part[:content])
  end
  File.write("#{out}/#{name}", content)
end

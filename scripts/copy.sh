ROOT=$(dirname $(readlink -f $0))/..
TUT="./src"
PUB="./tutorial-ssp"
DUM="./dummy"
ADJETIVA_REP="../plantillas-adjetiva-editorial"
ADJETIVA_TUT="$ADJETIVA_REP/tutorial/src"
ADJETIVA_PUB="$ADJETIVA_REP/tutorial/tutorial-ssp"
ADJETIVA_DUM="$ADJETIVA_REP/dummy"

cd $ROOT

# Copia archivos al dummy
rm -rf $DUM/static
rm -rf $DUM/assets
cp -r $TUT/static $DUM
mkdir -p $DUM/assets/img
cp $TUT/assets/img/portada.jpg $DUM/assets/img/

# Copia archivos al repositorio de Adjetiva
if [[ -d $ADJETIVA_REP ]]; then
  rm -rf $ADJETIVA_TUT $ADJETIVA_DUM
  cp -r $TUT $ADJETIVA_TUT
  cp -r $DUM $ADJETIVA_DUM
  cp README.md $ADJETIVA_REP/tutorial
  cp $PUB.html $ADJETIVA_PUB.html
  cp $PUB.epub $ADJETIVA_PUB.epub
  cp $PUB.sla $ADJETIVA_PUB.sla
fi

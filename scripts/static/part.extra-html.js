window.onload = () => {
  // Modifica apertura de enlaces externos
  modify_a();
  // El TOC por defecto es para quienes no tienen JS,
  // cuando hay soporte JS no se necesita ese TOC
  remove_default_toc();
  // Añade anclajes a los encabezados
  add_anchors();
  // Añade TOC
  add_toc();
  // Añade menú
  add_menu();
};

// Modifica comportamiento de enlaces
function modify_a () {
  modify_a_external();
  modify_a_internal();
}

// Modifica comportamiento de enlaces externos
function modify_a_external () {
  document.querySelectorAll('a[href*="//"]').forEach((a) => {
    a.target = '_blank';
  });
}

// Modifica comportamiento de enlaces internos
function modify_a_internal () {
  document.querySelectorAll('a').forEach((a) => {
    if (a.target != '_blank') {
      a.onclick = function(e) {
        e.preventDefault();
        let target = this.getAttribute('href');
        window.scrollTo({
          top: document.querySelector(target).offsetTop,
          behavior: "smooth"
        });
      };
    }
  });
}

// Elimina TOC por defecto
function remove_default_toc () {
  let old_toc = document.getElementById('toc');
  old_toc.parentNode.removeChild(old_toc);
}

// Añade iconos de anclaje en encabezados
function add_anchors () {
  anchors.options = {
    placement: 'left',
    visible: 'touch',
    icon: '§',
    class: 'anchor'
  }

  anchors.add('h1, h2, h3, h4, h5, h6');
}

// Añade TOC
function add_toc () {
  let nav = document.createElement('nav');

  nav.id = 'toc-nav';
  document.body.prepend(nav);

  tocbot.init({
    tocSelector: '#toc-nav',
    contentSelector: '#main',
    headingSelector: 'h1, h2, h3',
    ignoreHiddenElements: true,
    hasInnerContainers: true,
  });
}

// Añade menú cuando el TOC no cabe
function add_menu () {
  let background = build_menu_item('menu-background'),
    filler = build_menu_item('menu-filler', background),
    btn = build_menu_item('menu-btn'),
    bar3 = build_menu_item('menu-bar3', btn),
    bar2 = build_menu_item('menu-bar2', btn),
    bar1 = build_menu_item('menu-bar1', btn);

  // Resiva si tiene touch; cfr. https://stackoverflow.com/a/52855084
  if(window.matchMedia("(pointer: coarse)").matches) {
    btn.addEventListener('touchstart', toggle_menu);
    background.addEventListener('touchstart', toggle_menu);
  } else {
    btn.addEventListener('click', toggle_menu);
    background.addEventListener('click', toggle_menu);
  }

}

// Construye el elemento del menú
function build_menu_item (id, parent = document.body) {
  let item = document.createElement('div');
  item.id = id;
  if (parent) {
    parent.prepend(item);
  }
  return item;
}

// Muestra u oculta el menú
function toggle_menu () {
  let ids = ['toc-nav', 'menu-background', 'menu-btn'];
  ids.forEach((id) => {
    let item = document.getElementById(id);
    item.classList.toggle('menu-activo');
  });
}

ROOT=$(dirname $(readlink -f $0))/..
MAKE="./src/static/scripts/make.sh"

cd $ROOT

case $1 in
  --format)
    $MAKE -f -n
esac

$MAKE -o ../tutorial-ssp
./scripts/copy.sh

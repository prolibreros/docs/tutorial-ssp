# Agradecimientos {#agradecimientos epub:type="acknowledgments"}

Este tutorial no hubiera sido posible sin el financiamiento
de [Adjetiva Editorial], gracias por apostar por el
[*software* libre] para la producción de sus libros. Por
orden cronológico, agradezco a Mel Bayardo de [Programando
[LIBRE]{.acronimo}ros][1] por sus opiniones respecto a la
accesibilidad de este tutorial. También doy gracias a libsys
de [cuatrolibertades] por sus comentarios y correcciones al
[apéndice 3] que trata sobre el [*software* libre].

  [Adjetiva Editorial]: https://adjetiva.mx
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [1]: https://www.gitlab.com/prolibreros
  [cuatrolibertades]: https://cuatrolibertades.org
  [apéndice 3]: #apendice3

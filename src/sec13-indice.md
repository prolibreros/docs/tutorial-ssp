# Índice analítico {#indice epub:type="index"}

::: frances
[[GNU]{.acronimo}/Linux][1]

::: {.frances .sin-espacios}
[distribución]

::: {.frances .sin-espacios}
[Arch Linux]

[Debian]

[Fedora]

[Linux Mint]

[Ubuntu]
:::
:::
:::

::: frances
lenguajes computacionales

::: {.frances .sin-espacios}
[[CSL]]{.acronimo}

[Markdown]

[TeX]

[[YAML]]{.acronimo}

::: {.frances .sin-espacios}
[sitio oficial]

[validador]
:::
:::
:::

::: frances
organizaciones

::: {.frances .sin-espacios}
[Adjetiva Editorial]

[cuatrolibertades]

[epublibre]

[Programando [LIBRE]{.acronimo}ros][2]
:::
:::

::: frances
programas de cómputo

::: {.frances .sin-espacios}
[Calibre]

[EPUBCheck]

::: {.frances .sin-espacios}
[en línea]
:::

[Git]

[Gnome]

[Homebrew]

[Kindle Previewer]

[Pandoc]

::: {.frances .sin-espacios}
[instalación]

[manual]

[referencia de `defaults`]

[referencia de `epub:type`]
:::

[Scribus]

::: {.frances .sin-espacios}
[página de descarga]

[repositorio de versión 1.4.8]

[repositorio de versión 1.5.x]

[wiki]
:::

[Zettlr]

::: {.frances .sin-espacios}
[página de descarga][3]

[documentación]
:::
:::
:::

::: frances
publicaciones

::: {.frances .sin-espacios}
[*¿Qué es el* software *libre*?]

[*Elogio de los amanuenses*]

[*Goodbye, "free software"; hello, "open source"*]

[*La venta del* software *libre*]

[librito]

[*Por qué el «código abierto» pierde de vista lo esencial
del* software *libre*]
:::
:::

::: frances
términos editoriales

::: {.frances .sin-espacios}
[base de datos bibliográfica]

[*desktop publishing*]

[fe de erratas]

[[ISBN]]{.acronimo}

[paratexto]

[publicación desde una sola fuente]
:::
:::

::: frances
términos informáticos o relacionados

::: {.frances .sin-espacios}
[biblioteca o librería]

[*bugs*]

[capa de abstracción]

[código fuente]

[código máquina]

[fragmentos de texto reutilizables (*snippets*)]

[*hard-coded*]

[lenguaje de marcado ligero]

[metadatos]

[par clave-valor]

[*script*]

[*splash screen*]

[terminal]

[*trade-off*]
:::
:::

::: frances
términos jurídicos o relacionados

::: {.frances .sin-espacios}
[*copyleft*]

::: {.frances .sin-espacios}
[[GPL]]{.acronimo}

[lista de licencias]
:::

[cultura libre]

[derecho de autor (*copyright*)]

::: {.frances .sin-espacios}
[derechos morales]
:::
:::
:::

::: frances
tipos de *software*

::: {.frances .sin-espacios}
[código abierto]

::: {.frances .sin-espacios}
[definición de la Open Source Initiative]
:::

[*freemium*]

[*freeware*]

[*malware*]

[*software* libre]

::: {.frances .sin-espacios}
[filosofía]

[[FOSS/FLOSS]]{.acronimo}
:::

[*software* propietario]
:::
:::

::: frances
tutorial

::: {.frances .sin-espacios}
[en [EPUB]{.acronimo}][4]

[en línea][5]

[en [PDF]{.acronimo}][6]

[en [ZIP]{.acronimo}][7]

[repositorio]
:::
:::

  [1]: https://es.wikipedia.org/wiki/GNU/Linux
  [distribución]: https://es.wikipedia.org/wiki/Distribuci%C3%B3n_Linux
  [Arch Linux]: https://www.archlinux.org
  [Debian]: https://www.debian.org
  [Fedora]: https://getfedora.org
  [Linux Mint]: https://linuxmint.com
  [Ubuntu]: https://ubuntu.com
  [CSL]: https://es.wikipedia.org/wiki/Citation_Style_Language
  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [TeX]: https://es.wikipedia.org/wiki/TeX
  [YAML]: https://es.wikipedia.org/wiki/YAML
  [sitio oficial]: https://yaml.org
  [validador]: https://jsonformatter.org/yaml-validator
  [Adjetiva Editorial]: https://adjetiva.mx
  [cuatrolibertades]: https://cuatrolibertades.org
  [epublibre]: https://epublibre.org
  [2]: https://www.gitlab.com/prolibreros
  [Calibre]: https://calibre-ebook.com
  [EPUBCheck]: https://www.w3.org/publishing/epubcheck
  [en línea]: https://draft2digital.com/book/epubcheck/upload
  [Git]: https://es.wikipedia.org/wiki/Git
  [Gnome]: https://www.gnome.org
  [Homebrew]: https://brew.sh
  [Kindle Previewer]: https://kdp.amazon.com/es_ES/help/topic/G202131170
  [Pandoc]: https://pandoc.org
  [instalación]: https://pandoc.org/installing.html
  [manual]: https://pandoc.org/MANUAL.html
  [referencia de `defaults`]: https://pandoc.org/MANUAL.html#defaults-files
  [referencia de `epub:type`]: https://pandoc.org/MANUAL.html?pandocs-markdown#the-epubtype-attribute
  [Scribus]: https://www.scribus.net
  [página de descarga]: https://www.scribus.net/downloads/unstable-branch
  [repositorio de versión 1.4.8]: https://sourceforge.net/projects/scribus/files/scribus/1.4.8
  [repositorio de versión 1.5.x]: https://sourceforge.net/projects/scribus/files/scribus-devel/1.5.8
  [wiki]: https://wiki.scribus.net/canvas/Pagina_principal
  [Zettlr]: https://www.zettlr.com
  [3]: https://www.zettlr.com/download
  [documentación]: https://docs.zettlr.com/es
  [*¿Qué es el* software *libre*?]: https://www.gnu.org/philosophy/free-sw.es.html
  [*Elogio de los amanuenses*]: http://libgen.rs/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382
  [*Goodbye, "free software"; hello, "open source"*]: http://www.catb.org/~esr/open-source.html
  [*La venta del* software *libre*]: https://www.gnu.org/philosophy/selling.es.html
  [librito]: #librito
  [*Por qué el «código abierto» pierde de vista lo esencial del* software *libre*]:
    https://www.gnu.org/philosophy/open-source-misses-the-point.es.html
  [base de datos bibliográfica]: https://es.wikipedia.org/wiki/Base_de_datos_bibliogr%C3%A1fica
  [*desktop publishing*]: https://en.wikipedia.org/wiki/Desktop_publishing
  [fe de erratas]: https://inciclopedia.org/wiki/Fe_de_erratas
  [ISBN]: https://es.wikipedia.org/wiki/ISBN
  [paratexto]: https://es.wikipedia.org/wiki/Paratexto
  [publicación desde una sola fuente]: https://es.wikipedia.org/wiki/Publicaci%C3%B3n_desde_una_sola_fuente
  [biblioteca o librería]: https://es.wikipedia.org/wiki/Biblioteca_(inform%C3%A1tica)
  [*bugs*]: https://es.wikipedia.org/wiki/Error_de_software
  [capa de abstracción]: https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3n
  [código fuente]: https://es.wikipedia.org/wiki/C%C3%B3digo_fuente
  [código máquina]: https://es.wikipedia.org/wiki/Lenguaje_de_m%C3%A1quina
  [fragmentos de texto reutilizables (*snippets*)]: https://es.wikipedia.org/wiki/Snippet
  [*hard-coded*]: https://es.wikipedia.org/wiki/Hard_code
  [lenguaje de marcado ligero]: https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero
  [metadatos]: https://es.wikipedia.org/wiki/Metadatos
  [par clave-valor]: https://es.wikipedia.org/wiki/Base_de_datos_clave-valor
  [*script*]: https://es.wikipedia.org/wiki/Script
  [*splash screen*]: https://en.wikipedia.org/wiki/Splash_screen
  [terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [*trade-off*]: https://es.wikipedia.org/wiki/Trade-off
  [*copyleft*]: https://es.wikipedia.org/wiki/Copyleft
  [GPL]: https://es.wikipedia.org/wiki/GNU_General_Public_License
  [lista de licencias]: https://www.gnu.org/licenses/license-list.html
  [cultura libre]: https://es.wikipedia.org/wiki/Cultura_libre
  [derecho de autor (*copyright*)]: https://es.wikipedia.org/wiki/Derecho_de_autor
  [derechos morales]: https://es.wikipedia.org/wiki/Derechos_morales
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [definición de la Open Source Initiative]: https://opensource.org/osd
  [*freemium*]: https://es.wikipedia.org/wiki/Freemium
  [*freeware*]: https://es.wikipedia.org/wiki/Software_gratis
  [*malware*]: https://es.wikipedia.org/wiki/Malware
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [filosofía]: https://www.gnu.org/philosophy/philosophy.es.html
  [FOSS/FLOSS]: https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto
  [*software* propietario]: https://es.wikipedia.org/wiki/Software_propietario
  [4]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.epub
  [5]: https://prolibreros.gitlab.io/docs/tutorial-ssp
  [6]: https://prolibreros.gitlab.io/docs/tutorial-ssp/tutorial-ssp.pdf
  [7]: https://gitlab.com/prolibreros/docs/tutorial-ssp/-/archive/no-masters/tutorial-ssp-no-masters.zip
  [repositorio]: https://gitlab.com/prolibreros/docs/tutorial-ssp

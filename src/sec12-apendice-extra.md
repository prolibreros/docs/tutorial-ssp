# Apéndice 5. Elementos adicionales {#apendice5 epub:type="appendix"}

A continuación se muestras elementos adicionales de la
sintaxis [Markdown] soportada por [Pandoc] que no aparece en
el resto del documento.

## `Cite`

Citado [@id1; @id2; @id3]

## `DefinitionList`

Término 1

:   Definición 1

Término 2 con *marcado en línea*

:   Primer párrafo de la definición del término 2

        { línea de código como segundo párrafo de la definición del término 2 }

    Tercer párrafo de la definición del término 2.

## `DoubleQuoted`

''Entrecomillado inglés''

## `HorizontalRule`

------------------------------------------------------------

## `LineBlock`

| Elemento
|        LineBlock

## `LineBreak`

Corte\
de línea

## `SingleQuoted`

'Entrecomillado simple'

## `Strikeout`

~~Tachado~~

## `Subscript`

~Subíndice~

## `Superscript`

^Superíndice^

  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [Pandoc]: https://pandoc.org

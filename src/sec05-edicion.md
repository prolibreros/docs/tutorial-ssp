# Edición del [YAML]{.acronimo} y los Markdown {#edicion epub:type="chapter"}

## Edición de los metadatos

Los [metadatos] de un libro describen la información sobre
la obra como su título, colaboradores, fecha de publicación
o cantidad de ediciones. Los [metadatos] son relevantes para
la identificación del archivo y son útiles para localizar la
publicación cuando está indexada en una biblioteca, base de
datos o motor de búsqueda.

Para nuestro [librito] escribiremos sus [metadatos] en
formato [[YAML]]{.acronimo} en el archivo `metadata.yaml`.
[[YAML]]{.acronimo} es un lenguaje para la notación de
objetos que [Pandoc] y [Zettlr] usan para la escritura de
los metadatos de una publicación. Para comenzar en
[Zettlr][]:

1.  Ve a `Archivo > Abrir área de trabajo…`.
2.  Selecciona la carpeta [`librito`][librito].
3.  Da clic sobre el nombre [`librito`][librito] ubicado en
    el panel izquierdo.
4.  Da clic sobre el archivo `metadata.yaml`.

::: {.note .centrado}
La edición de los [metadatos] también puedes hacerla en tu
editor de preferencia.
:::

En general un archivo [[YAML]]{.acronimo} se compone de la
declaración de uno o más objetos, también conocidos como
registros. Cada objeto es un [par clave-valor], en donde

-   la clave (*key*) identifica al registro y
-   el valor (*value*) es la información del registro.

En [[YAML]]{.acronimo} un [par clave-valor] se registra con
la siguiente sintaxis:

``` yaml
clave: valor
```

Por ejemplo, en `metadata.yaml` el título del [librito] se
registra en:

``` yaml
title: Un título
```

::: warn
[Zettlr] usa el nombre del proyecto como el título del
[librito]. El registro `title` en `metadata.yaml` se usa
cuando el [librito] se produce afuera de [Zettlr]. Por
interoperabilidad es recomendable declarar el título del
proyecto en ambos lugares.
:::

En la mayoría de los casos los [metadatos] del [librito] se
declaran con esa sintaxis. Sin embargo, hay registros que
requieren un conjunto de información. Para esos casos la
sintaxis es:

``` yaml
clave:
- ítem 1
- ítem 2
- ítem 3
```

Por ejemplo, en `metadata.yaml` las palabras clave del
[librito] se registran en:

``` yaml
keywords:
- palabra clave 1
- otra palabra clave
```

En otros casos la información del registro puede a su vez
contener más [pares clave-valor][par clave-valor]. Para
estos casos, la sintaxis es:

``` yaml
clave:
  una_clave: valor 1
  otra_clave: valor 2
```

Por ejemplo, en `metadata.yaml` los [[ISBN]]{.acronimo} del
[librito] se registran en:

``` yaml
isbn:
  web: 978-607-99-8760-2
  ebook: 978-607-99-8760-2
  print: 978-607-99-8760-2
```

::: note
En teoría el [[ISBN]]{.acronimo} de una publicación
corresponde a cada uno de sus soportes, por lo que debería
ser un [[ISBN]]{.acronimo} distinto para el formato *web*,
*ebook* e impreso. Sin embargo, en la práctica esta
indiación tiende a ser omitida y en su lugar se emplea el
mismo [[ISBN]]{.acronimo} para todos los soportes.
:::

Para evitar errores de procesamiento, cuando el valor de un
registro contiene dos puntos (`:`) o [HTML]{.acronimo}, la
sintaxis de este valor se escapa con comillas simples (`'`):

``` yaml
clave: 'valor con dos puntos: y <b>HTML</b>'
```

Por ejemplo, en `metadata.yaml` los datos de producción y
del directorio del [librito] se registran en:

``` yaml
made-in: 'Hecho en México · <i>Made in Mexico</i>'
directory:
- 'Autor: Juan Pérez'
- 'Editora: Juana Pérez'
- 'Diseño de portada: Paco'
```

Por último, el caso más complejo de registro en
`metadata.yaml` es para el [metadato][metadatos] del
[EPUB]{.acronimo} que se usa para registrar a los creadores.
Este registro es un conjunto en donde cada ítem está
compuesto por las claves `role` y `text` que se usan para
indicar el rol y nombre de cada creador. Su sintaxis es:

``` yaml
creator:
- role: author
  text: Juan Pérez
- role: editor
  text: Juana Pérez
- role: cover-designer
  text: Paco
```

Por fortuna, no tienes que adentrarte mucho en la sintaxis
del formato [[YAML]]{.acronimo}. Solo requieres modificar
los registros con tu información, eliminar los que no
necesites, o copiar y pegar más ítems para los registros que
son conjuntos. Si todos tienen sintaxis correcta, los
registros van a ser utilizados como [metadatos] para nuestro
[librito].

::: {.note .centrado}
Para validar tus [metadatos] [[YAML]]{.acronimo} puedes usar
[este sitio].
:::

La modificación de metadatos puedes hacerla en cualquier
momento durante el proceso de edición, solo recuerda
[exportar] el proyecto para implementar los cambios. Ahora
podemos seguir con la edición de los contenidos.

::: {.note .centrado}
Si te interesa saber más sobre [[YAML]]{.acronimo}, visita
[su sitio].
:::

## Edición del contenido

::: {.warn .centrado}
En proceso de redacción
:::

Para la fuente de nuestro libro vamos a utilizar [Markdown],
un [lenguaje de marcado ligero] cuya filosofía es:

-   Fácil de leer
-   Fácil de escribir
-   Fácil de procesar

[Markdown] es un formato de archivo abierto, por lo que
puede trabajarse en cualquier editor de texto o de código.

Sin embargo, por conveniencia vamos a utlizar [Zettlr], un
programa libre para la edición de [Markdown].

::: {.note .centrado}
No olvides visitar la [documentación de Zettlr].
:::

```{=html}
<!--
Este es un comentario. 
Los comentarios no son visibiles en las conversiones.
Los comentarios son útiles para discutir sobre el libro.
-->
```
```{=html}
<!--
Para un EPUB podemos asignar nombres de secciones con el atributo 'epub:type'.
Para ver más nombres de secciones, visita:
-->
```
Para comenzar en [Zettlr][]:

1.  Ve a `Archivo > Abrir área de trabajo…`.
2.  Selecciona la carpeta [`librito`][librito].
3.  Da clic sobre el nombre [`librito`][librito] ubicado en
    el panel izquierdo.
4.  Da clic sobre el archivo `sec01`.

::: {.note .centrado}
La edición de los [Markdown] también puedes hacerla en tu
editor de preferencia.
:::

  [metadatos]: https://es.wikipedia.org/wiki/Metadatos
  [librito]: #librito
  [YAML]: https://es.wikipedia.org/wiki/YAML
  [Pandoc]: https://pandoc.org
  [Zettlr]: https://www.zettlr.com
  [par clave-valor]: https://es.wikipedia.org/wiki/Base_de_datos_clave-valor
  [ISBN]: https://es.wikipedia.org/wiki/ISBN
  [este sitio]: https://jsonformatter.org/yaml-validator
  [exportar]: #exportacion
  [su sitio]: https://yaml.org
  [Markdown]: https://es.wikipedia.org/wiki/Markdown
  [lenguaje de marcado ligero]: https://es.wikipedia.org/wiki/Lenguaje_de_marcas_ligero
  [documentación de Zettlr]: https://docs.zettlr.com/es

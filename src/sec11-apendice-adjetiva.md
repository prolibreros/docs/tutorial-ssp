# Apéndice 4. Estilos de Adjetiva Editorial {#apendice4 epub:type="appendix"}

::: {.warn .centrado}
A continuación se muestran los estilos para los libros de
[Adjetiva Editorial].
:::

::: primer-parrafo
[P]{.capitular}rimer párrafo. Lorem ipsum dolor sit amet.
Quel ramo del lago fr. La costiera, formata dal deposito di
tre grossi torrenti, scende appoggiata a due monti contigui,
l'uno detto di san Martino, l'altro, con voce lombarda, il
Resegone, dai molti suoi cocuzzoli in fila, che in vero lo
fanno somigliare a una sega: talché non è chi, al primo
vederlo, purché sia di fronte, come per esempio di su le
mura di Milano che guardano a settentrione, non lo discerna
tosto, a un tal contrassegno, in quella lunga e vasta
giogaia, dagli altri monti di nome più oscuro e di forma più
comune. Aliquam aliquet purus molestie dolor. Integer quis
eros ut erat posuere dictum. Curabitur dignissim. Integer
orci. Fusce vulputate lacus at ipsum. Quisque in libero nec
mi laoreet volutpat. Aliquam eros pede, scelerisque quis,
tristique cursus, placerat convallis, velit. Nam
condimentum. Nulla ut mauris. Curabitur adipiscing, mauris
non dictum aliquam, arcu risus dapibus diam, nec
sollicitudin quam erat quis ligula. Aenean massa nulla,
volutpat eu, accumsan et, fringilla eget, odio. Nulla
placerat porta justo. Nulla vitae turpis. Praesent lacus.
:::

Cuerpo de texto. Consectetuer adipiscing elit. Pellentesque
habitant morbi tristique senectus et netus et malesuada
fames ac turpis egestas. Quisque vel erat eget diam
consectetuer iaculis. Cras ante velit, suscipit et, porta
tempus, dignissim quis, magna. Vivamus viverra, turpis nec
rhoncus ultricies, diam turpis eleifend nisl, a eleifend
ante felis ac sapien. Integer bibendum. Suspendisse in mi
non neque bibendum convallis. Suspendisse potenti. Sed sit
amet purus at felis adipiscing aliquam. Vivamus et nisl sit
amet mauris aliquet molestie. Integer tortor massa, aliquam
a, lacinia nonummy, sagittis nec, eros. Nunc non mauris id
eros venenatis adipiscing. Cras et lectus ut nisl pharetra
ornare. Proin leo risus, elementum eget, ultrices vitae,
molestie sed, erat. Curabitur et lectus in tellus egestas
hendrerit. Sed dapibus ipsum. Quisque sit amet ligula.
Suspendisse odio dolor, semper id, feugiat quis, sodales id,
mauris. Curabitur id ligula ac libero malesuada pharetra.

## Encabezado 2

::: sin-sangria
Párrafo sin sangría. Suspendisse nibh. Nunc vulputate leo id
urna. Donec dictum. Lorem ipsum dolor sit amet, consectetuer
adipiscing elit. Lorem ipsum dolor sit amet, consectetuer
adipiscing elit. Suspendisse dictum, magna consectetuer
hendrerit volutpat, sapien felis faucibus justo, ac dictum
lacus pede in metus. Nam commodo. Sed consequat, leo pretium
sagittis congue, ante nunc laoreet nisl, ac aliquam risus
tellus commodo elit. Pellentesque suscipit erat vitae
mauris. Sed iaculis eros vitae mauris. Vestibulum ante ipsum
primis in faucibus orci luctus et ultrices posuere cubilia
Curae; Suspendisse id ante et elit accumsan semper. Sed et
nibh eget purus scelerisque volutpat. Sed mi. Proin tellus
felis, tincidunt eget, dictum et, adipiscing et, urna. Cras
accumsan diam sed turpis. Etiam sollicitudin lacus.
:::

> Bloque de cita. Cras ut mi sit amet quam consequat
> consequat. Aenean ut lectus. Cum sociis natoque penatibus
> et magnis dis parturient montes, nascetur ridiculus mus.
> Suspendisse vel sapien. Nullam non turpis. Pellentesque
> elementum pharetra ligula. In rhoncus. Aliquam vel enim
> consequat sem aliquet hendrerit. Lorem ipsum dolor sit
> amet, consectetuer adipiscing elit. Nam felis.

*Unas itálicas*. Cursus vel, sagittis a, dolor. Nullam
turpis lacus, ultrices vel, sagittis vitae, dapibus vel,
elit. Suspendisse auctor, sapien et suscipit tempor, turpis
enim consequat sem, eu dictum nunc lorem at massa.
Pellentesque scelerisque purus. Etiam sed enim. Maecenas sed
tortor id turpis consequat consequat. Curabitur fringilla.
Sed risus wisi, dictum a, sagittis nec, luctus ac, neque.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Sed nibh neque, aliquam ut, sagittis id, gravida et, est.
Aenean consectetuer pretium enim.

### Encabezado 3

**Unas negritas**. Nunc vulputate leo id urna. Donec dictum.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
Suspendisse dictum, magna consectetuer hendrerit volutpat,
sapien felis faucibus justo, ac dictum lacus pede in metus.
Nam commodo. Sed consequat, leo pretium sagittis congue,
ante nunc laoreet nisl, ac aliquam risus tellus commodo
elit. Cras at elit. Pellentesque suscipit erat vitae mauris.

-   Lista no numerada. Vivamus neque velit, ornare vitae,
    tempor vel, ultrices et, wisi.
    -   Segundo nivel. Phasellus nunc turpis, cursus non,
        rhoncus vitae, sollicitudin vel, velit.
-   Cras pede. Vivamus suscipit lorem sed felis. Vestibulum
    vestibulum ultrices turpis.

***Unas itálicas con negritas***. Lorem ipsum dolor sit
amet, elit. Praesent ornare nulla nec justo. Sed nec risus
ac risus fermentum vestibulum. Etiam viverra viverra sem.
Etiam molestie mi quis metus hendrerit tristique.

#### Encabezado 4

[Unas versalitas]{.versalita}. Suspendisse potenti. Cras ut
mi sit amet quam consequat consequat. Aenean ut lectus. Cum
sociis natoque penatibus et magnis dis parturient montes,
nascetur ridiculus mus. Suspendisse vel sapien. Nullam non
turpis. Pellentesque elementum pharetra ligula. Lorem ipsum
dolor sit amet, consectetuer adipiscing elit. Nam felis.

1.  Lista numerada. Vivamus neque velit, ornare vitae,
    tempor vel, ultrices et, wisi. Cras pede.
    1.  Segundo nivel. Phasellus nunc turpis, cursus non,
        rhoncus vitae, sollicitudin vel, velit.
2.  Vivamus suscipit lorem sed felis.

[Unas versales]{.versal}. Vestibulum vestibulum ultrices
turpis. Lorem ipsum dolor sit amet, consectetuer adipiscing
elit. Praesent ornare nulla nec justo. Sed nec risus ac
risus fermentum vestibulum. Etiam viverra viverra sem. Etiam
molestie mi quis metus hendrerit tristique.

##### Encabezado 5

[Un resaltado]{.resaltado}. Vivamus neque velit, ornare
vitae, tempor vel, ultrices et, wisi.[^1] Cras pede.
Phasellus nunc turpis, cursus non, rhoncus vitae,
sollicitudin vel, velit. Vivamus suscipit lorem sed
felis.[^2] Vestibulum vestibulum ultrices turpis. Lorem
ipsum dolor sit amet, consectetuer adipiscing elit. Praesent
ornare nulla nec justo. Sed nec risus ac risus fermentum
vestibulum. Etiam viverra viverra sem. Etiam molestie mi
quis metus hendrerit tristique.

::: derecha
Párrafo a la derecha. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Nam commodo. Sed consequat,
leo pretium sagittis congue, ante nunc laoreet nisl, ac
aliquam risus tellus commodo elit. Cras at elit.
Pellentesque mauris.
:::

::: centrado
Párrafo centrado. Suspendisse id ante et elit accumsan
semper. Sed et nibh eget purus scelerisque volutpat. Sed mi.
Proin tellus felis, tincidunt eget, dictum et, adipiscing
et, urna. Cras accumsan diam sed turpis. Etiam sollicitudin
lacus.
:::

###### Encabezado 6

::: epigrafe
*Un epígrafe. Suspendisse nibh. Nunc vulputate leo id urna
donec dictum. Lorem ipsum dolor sit amet.*`<br/>`{=html}
[Nombre Apellido]{.versalita}
:::

::: justificado
Párrafo justificado. Nulla facilisi. Nam varius ante
dignissim arcu. Suspendisse molestie dignissim neque.
Suspendisse leo ipsum, rutrum cursus, malesuada id, dapibus
sed, urna. Fusce sollicitudin laoreet diam. Mauris eu quam
eget nulla fermentum adipiscing. In hac habitasse platea
dictumst. Morbi ut odio vitae eros luctus luctus. Ut diam.
Phasellus ullamcorper arcu vitae wisi.
:::

::: frances
Párrafo francés. Nulla facilisi. Nam varius ante dignissim
arcu. Suspendisse molestie dignissim neque. Suspendisse leo
ipsum, rutrum cursus, malesuada id, sed, urna.
:::

::: bib
1.  Bibliografía. In hac habitasse platea dictumst. Morbi ut
    odio vitae eros luctus luctus. Ut diam. Phasellus
    ullamcorper arcu vitae wisi.

::: bib-raya
-   Bibliografía sin numerar. In hac habitasse platea
    dictumst. Morbi ut odio vitae eros luctus luctus. Ut
    diam. Phasellus ullamcorper arcu vitae wisi.
:::

2.  Bibliografía. In hac habitasse platea dictumst. Morbi ut
    odio vitae eros luctus luctus. Ut diam. Phasellus
    ullamcorper arcu vitae wisi.
:::

::: recuadro
Un recuadro. Nam iaculis blandit purus. Mauris odio nibh,
hendrerit id, cursus vel, sagittis a, dolor. Nullam turpis
lacus, ultrices vel, sagittis vitae, dapibus vel, elit.
Suspendisse auctor, sapien et suscipit tempor, turpis enim
consequat sem, eu dictum nunc lorem at massa. Pellentesque
scelerisque purus. Etiam sed enim. Maecenas sed tortor id
turpis consequat consequat. Curabitur fringilla. Sed risus
wisi, dictum a, sagittis nec, luctus ac, neque. Lorem ipsum
dolor sit amet, consectetuer adipiscing elit.

[Un resaltado fuerte]{.resaltado-fuerte}. Sed nibh neque,
aliquam ut, sagittis id, gravida et, est. Aenean
consectetuer pretium enim. Aenean tellus quam, condimentum
a, adipiscing et, lacinia vel, ante. Praesent faucibus
dignissim enim. Aliquam tincidunt.
:::

Una fórmula matemática embebida:

$$
J = \int r^{2}dm
$$

::: legal
Legal. ipsum dolor sit amet, consectetuer adipiscing elit.
Ut a sapien. Nulla placerat porta justo. Nulla vitae turpis.
Praesent lacus.
:::

::: colofon
Colofón. Pellentesque velit. Aliquam erat volutpat. Duis
sagittis nibh sed justo. Proin facilisis. Aliquam sagittis
lacinia mi. Aliquam eu nulla at turpis vulputate hendrerit.
Proin at diam. Curabitur euismod.
:::

::: {.warn .centrado}
A partir de aquí hay divergencias con los estilos de
Scribus.
:::

![**Un pie de imagen**. Divergencia: adición de borde
inferior y márgenes a la izquierda y derecha, puede
implementarse en Scribus.]

Una fórmula matemática en la misma línea:
$J = \int r^{2}dm$.

::: espacios
Una imagen en la misma línea: ![][1].
:::

::: espacios
Las imágenes en la misma línea es algo que tiene que hacerse
manualmente en Scribus, mientras que aquí solo se requiere
añadir el código para fórmulas LaTeX en línea o para
imágenes.
:::

::: espacio-arriba
Una tabla nativa:
:::

::: centrado
**Nombre de la tabla**
:::

   Encabezado 1   Encabezado 2   Encabezado 3
  -------------- -------------- --------------
     Celda 1        Celda 2        Celda 3
     Celda 4        Celda 5        Celda 6

Aquí es posible tener las tablas nativas como son las tablas
embebidas en Scribus. Esta tabla nativa se puede usar en
Scribus para sus tablas nativas aunque el estilo difiere
significativamente.

[^1]: Una nota al pie. Phasellus ullamcorper arcu vitae
    wisi. Pellentesque urna odio, varius eget, dignissim
    quis, vehicula placerat, nunc.

[^2]: Otra nota al pie. Nullam non turpis. Pellentesque
    ligula. Etiam viverra viverra sem. Etiam molestie mi
    quis metus hendrerit.

  [Adjetiva Editorial]: https://adjetiva.mx
  [**Un pie de imagen**. Divergencia: adición de borde inferior y márgenes a la izquierda y derecha, puede implementarse en Scribus.]:
    assets/img/volcan.jpg
    "**Un pie de imagen**. Divergencia: adición de borde inferior y márgenes a la izquierda y derecha, puede implementarse en Scribus."
  [1]: assets/img/volcan.jpg

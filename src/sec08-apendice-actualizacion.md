# Apéndice 1. Actualización del proyecto {#apendice1 epub:type="appendix"}

Cada proyecto hecho con este tutorial empieza con un
proyecto base llamado «*dummy*». El *dummy* cuenta con una
carpeta `static` en el que hay archivos necesarios para la
producción de nuestros formatos.

Estos archivos están en continua mejora, para poder
incorporarlos se tiene que actualizar el proyecto. La
actualización también funciona en caso de que se necesiten
revertir los cambios realizados a los archivos dentro de
`static`.

Para actualizar el proyecto haz lo siguiente:

1.  Entra a la carpeta de tu proyecto.
2.  Respalda la carpeta `static/img` afuera de la carpeta
    `static`.
3.  Elimina la carpeta `static`.
4.  [Descarga este tutorial] en [ZIP]{.acronimo}.
5.  Descomprime el archivo `tutorial-ssp-no-masters.zip`.
6.  Entra a la carpeta `tutorial-ssp-no-masters/dummy`.
7.  Copia la carpeta `static`.
8.  Pega esta carpeta dentro de tu proyecto.
9.  Mueve la carpeta `img` respaldada dentro de `static`.

::: warn
Si por algún motivo no puedes descargar el tutorial en
[ZIP]{.acronimo}, entonces:

1.  Ve a su [repositorio].
2.  Da clic en el botón ![][1] de descarga.
3.  Selecciona `zip`.
:::

::: note
Si cuentas con [Git], prefiere este método de descarga con:

    git clone --depth 1 https://gitlab.com/prolibreros/docs/tutorial-ssp.git
:::

El resultado final debe ser una nueva carpeta `static` en tu
proyecto que adentro tiene la carpeta `img` con tus
imágenes.

Con esto terminamos la actualización y ahora puedes
[exportar] tu proyecto para implementar los cambios.

  [Descarga este tutorial]: https://gitlab.com/prolibreros/docs/tutorial-ssp/-/archive/no-masters/tutorial-ssp-no-masters.zip
  [repositorio]: https://gitlab.com/prolibreros/docs/tutorial-ssp
  [1]: assets/img/btn-descarga.jpg
  [Git]: https://es.wikipedia.org/wiki/Git
  [exportar]: #exportacion

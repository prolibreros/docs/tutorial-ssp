# Exportación del [HTML]{.acronimo} y el [EPUB]{.acronimo} {#exportacion epub:type="chapter"}

La exportación del [librito] puede hacerse en [Zettlr] o en
la [terminal] de tu preferencia. En esta sección se describe
cómo llevarlo acabo en ambas interfaces.

## Exportación en Zettlr

En la interfaz de [Zettlr] la exportación es posible porque
bajo el capote tiene [Pandoc], un programa libre para la
conversión de documentos.

El [EPUB]{.acronimo} puede subirse a las tiendas de Google,
Apple y Amazon, así como puede convertirse para Kindle ---a
través de [Kindle Previewer]--- si se quiere ofrecer el
[librito] afuera de Amazon. El [HTML]{.acronimo} es para
cotejo rápido y, si se desea, para tener el [librito] en un
micrositio *web*, como este tutorial. El [SLA]{.acronimo} es
para hacer la maquetación del [PDF]{.acronimo} en el
[siguiente apartado].

Para exportar los formatos en [Zettlr][]:

1.  Ve a `Archivo > Abrir área de trabajo…`.
2.  Selecciona la carpeta [`librito`][librito].
3.  Da clic derecho sobre el nombre [`librito`][librito]
    ubicado en el panel izquierdo.
4.  Selecciona `Exportar proyecto`.

![Vista al hacer clic derecho en [librito].][1]

Después de unos segundos, en el explorador de archivos y
dentro de la carpeta de tu [librito] verás 6 archivos
nuevos:

``` sh
librito
├── librito.html
├── librito.rtf
├── librito.txt
├── log.epub
├── log.html
├── log.sla
...
```

Los archivos `log.*` son las bitácoras de exportación de
cada formato. Estos archivos son útiles para depurar errores
en la exportación así que vale la pena conservarlos.

Los archivos en formato [HTML]{.acronimo}, [RTF]{.acronimo}
y [TXT]{.acronimo} corresponden a la publicación en
[HTML]{.acronimo}, [EPUB]{.acronimo} y [SLA]{.acronimo}
---proyecto de Scribus---. Para mi caso estos archivos
tienen por nombre [`librito.*`][librito]. Para tu caso sus
nombres corresponden al título de tu publicación.

Si recuerdas, en la [configuración de Zettlr] utilizamos la
exportación `RTF` para colocar la configuración de
exportación para el [EPUB]{.acronimo}, así como la
exportación `Plain Text` se usó para la configuración de
exportación del proyecto de Scribus. Por estos *hacks*, en
tu explorador de archivos:

1.  Renombra la extensión `.rtf` de tu [librito] a `.epub`.
2.  Renombra la extensión `.txt` de tu [librito] a `.sla`.

![A la izquierda, vista del explorador de archivos con los
archivos hechos por [Zettlr]. A la derecha, misma vista pero
con los archivos renombrados.][2]

Con estos cambios ya puedes ver tu [librito] en tu
explorador *web* de preferencia si desde ahí abres el
archivo [HTML]{.acronimo}. También puedes ver el
[EPUB]{.acronimo} en tu *ereader*.

::: note
Si careces de *ereader*, [Calibre] es la recomendación. Este
programa libre es un gestor multiplataforma de *ebooks*. Una
de sus principales ventajas frente a otros *ereaders*, como
Adobe Digital Editions o iBooks, es que da seguimiento a los
estándares del [EPUB]{.acronimo}, lo que permite una mejor
visualización de las publicaciones.
:::

::: {.note .centrado}
El [EPUB]{.acronimo} puedes validarlo en [este sitio] que
bajo el capote usa [EPUBCheck].
:::

Este proceso puedes repetirlo cuantas veces sean necesarias.
Cuando la edición haya terminado y los resultados de la
exportación sean los esperados, es momento de continuar con
la maquetación de nuestro [librito].

::: {.error .centrado}
Antes de volver a exportar el [librito], asegúrate de
eliminar los archivos previos; de lo contrario tendrás
errores de visualización.
:::

::: {.error .centrado}
La exportación genera un nuevo documento [SLA]{.acronimo}
que sobreescribe los cambios realizados; haz la maquetación
hasta que el [HTML]{.acronimo} y [EPUB]{.acronimo} estén
listos.
:::

## Exportación en la terminal

Si has trabajado el [librito] en tu editor de preferencia en
lugar de [Zettlr], puedes exportar la fuente en tu
[terminal]. Para este proceso es necesario que tengas
instalado [Pandoc].

::: {.warn .centrado}
Visita [esta página] para leer las instrucciones de
instalación de [Pandoc].
:::

Para exportar, en tu [terminal][]:

1.  Escribe `cd` (**con el espacio al final**).
2.  Arrastra la carpeta del [librito] a la [terminal] y da
    `Enter`.
3.  Ejecuta `./static/scripts/make.sh`.

El archivo que se ejecuta, `make.sh`, es un [*script*] que
realiza las operaciones necesarias con [Pandoc] para
producir las salidas. Las exportaciones solo requieren la
ejecución de ese [*script*], por lo que su producción en la
[terminal] es lo más ameno posible.

Después de unos segundos, en tu [terminal] verás 6 archivos
nuevos si haces `ls`:

``` sh
librito
├── log.epub
├── log.html
├── log.sla
...
├── pub.html
├── pub.rtf
├── pub.txt
...
```

Los archivos `log.*` son las bitácoras de exportación de
cada formato. Estos archivos son útiles para depurar errores
en la exportación así que vale la pena conservarlos.

Los archivos en formato [HTML]{.acronimo}, [EPUB]{.acronimo}
y [SLA]{.acronimo} ---proyecto de Scribus--- son las salidas
de tu [librito]. Por defecto todos tienen por nombre
`pub.*`.

El [*script*] `make.sh` ofrece algunas opciones ---como
asignar un nombre de salida distinto a `pub.*`--- que puedes
leerlas si ejecutas con la opción `-h`:

    ./static/scripts/make.sh -h

Si los resultados no te satisfacen, siente la libertad de
modificar el uso de [Pandoc] en el [*script*]. Cualquier
cambio puede ser revertido.

::: {.note .centrado}
No olvides visitar el [manual de Pandoc] para tus *hacks*.
:::

::: {.note .centrado}
Para revertir los cambios, consulta el [apéndice 1].
:::

  [librito]: #librito
  [Zettlr]: https://www.zettlr.com
  [terminal]: https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos
  [Pandoc]: https://pandoc.org
  [Kindle Previewer]: https://kdp.amazon.com/es_ES/help/topic/G202131170
  [siguiente apartado]: #maquetacion
  [1]: assets/img/zettlr-exportar.jpg
  [configuración de Zettlr]: #configuración-de-zettlr
  [2]: assets/img/zettlr-renombrar.jpg
  [Calibre]: https://calibre-ebook.com
  [este sitio]: https://draft2digital.com/book/epubcheck/upload
  [EPUBCheck]: https://www.w3.org/publishing/epubcheck
  [esta página]: https://pandoc.org/installing.html
  [*script*]: https://es.wikipedia.org/wiki/Script
  [manual de Pandoc]: https://pandoc.org/MANUAL.html
  [apéndice 1]: #apendice1

# Colofón {#colofon .oculto epub:type="colophon"}

::: colofon
Este documento fue hecho para [Adjetiva Editorial] por parte
de Perro Tuerto, integrante de [Programando
[LIBRE]{.acronimo}ros][1]. Para su composición se usó puro
*software* libre o de código abierto: [Arch Linux] con
[Gnome], [Pandoc], [Scribus] y [Zettlr].
:::

------------------------------------------------------------

Una prueba.[^1]

::: test
    sh test

> [Programando [***LIBRE***]{.acronimo
> llave="valor"}[ros]{#id k="v"}][1]
:::

[^1]: [Programando [***LIBRE***]{.acronimo
    llave="valor"}[ros]{k="v"}][1]

    # Algo más de nota

  [Adjetiva Editorial]: https://adjetiva.mx
  [1]: https://www.gitlab.com/prolibreros
  [Arch Linux]: https://www.archlinux.org
  [Gnome]: https://www.gnome.org
  [Pandoc]: https://pandoc.org
  [Scribus]: https://www.scribus.net
  [Zettlr]: https://www.zettlr.com

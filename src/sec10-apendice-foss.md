# Apéndice 3. *Software* de uso libre {#apendice3 epub:type="appendix"}

Las herramientas que usamos en este tutorial ([Zettlr],
[Pandoc] y [Scribus]) son *software* libre. Si eres una
persona que de manera profesional se dedica a la edición de
libros, podría causarte interés o sospecha la posibilidad de
hacer libros con calidad editorial sin Adobe ni Microsoft
Office. Algunas de las preguntas que podrías tener son:

-   ¿Qué es el [*software* libre]?
-   ¿Cuáles son las posibilidades y los límites del
    [*software* libre]?
-   ¿Cuál es la conveniencia de usar herramientas que son
    [*software* libre]?

La libertad en el uso de *software* se entiende de distintas
maneras. La primera de ellas es comprender al [*software*
libre] como un programa que permite la distribución de forma
gratuita ([*freeware*]).

Si bien es usual que el [*software* libre] sea distribuído
gratuitamente, es relevante hacer la distinción respecto al
[*freeware*]. Las licencias de [*software* libre] exigen
tener el [código fuente] disponible, mientras que en el
[*freeware*] es suficiente con tener disponible el programa
ejecutable ([código máquina]).

La diferencia entre el [código fuente] y el [código máquina]
es que en la gran mayoría de los casos el [código fuente]
está escrito en algún lenguaje de programación. Estos
lenguajes están pensados para ser inteligibles por humanos.
Antes de los lenguajes de programación, el *software* se
programaba con ceros y unos, una representación que era
difícil de escribir, entender, mantener y arreglar.

![Comparación entre el [código fuente] y el [código
máquina]. En este ejemplo el [código fuente] escrito en C++
indica que imprima un `hi` durante su ejecución en la
terminal. El proceso de compilación traduce este [código
fuente] a [código máquina] disponible en un archivo
ejecutable donde el mismo programa queda representado por
ceros y unos.][1]

El [*freeware*] solo tiene disponible el [código máquina]
para su ejecución, por lo que es difícil de modificar o
auditar. Por lo general esto es conveniente para el
desarrollador de [*freeware*], ya que le permite implementar
características indeseables para el usuario, como el uso de
publicidad, el monitero de su actividad o la instalación de
*software* malicioso ([*malware*]). Cuando un programa
gratuito es además [*software* libre], estas características
indeseables son fáciles de detectar y corregir porque su
[código fuente] permite el escrutinio.

::: note
El [*freeware*] y el [*software* libre] son distintos. El
[*freeware*] son programas ejecutables disponibles sin costo
pero sin la certeza de cómo funcionan. El [*software* libre]
son programas, usualmente gratuitos, que tienen su [código
fuente] disponible, lo que permite tener certeza sobre su
funcionamiento.
:::

Aunque el [*software* libre] suele estar disponible de
manera gratuita por internet, su gratuidad [no es requisito]
para considerarse libre. El [*software* libre] puede ser
gratuito o de pago, pero su usuario siempre debe contar con
el derecho de compartirlo libremente, además de otras
libertades que se describen más adelante.

::: espacio-arriba
La segunda manera de comprender al [*software* libre] es
como un programa de [código abierto].
:::

En el ejercicio habitual de desarrollo y uso de *software*
son poco perceptibles las diferencias entre el [código
abierto] y el [*software* libre]. En ambos tipos de
programas se cuenta con el [código fuente] disponible y con
licencias de uso que permiten su estudio, modificación,
reutilización o distribución sin costo adicional y sin la
anuencia por escrito del titular de los derechos.

::: note
En la práctica el [*software* libre] suele ser [código
abierto] y viceversa, por esto es común denominarlos con el
acrónimo [[FOSS]]{.acronimo} (*Free and Open-Source
Software*) o [[FLOSS][FOSS]]{.acronimo} (*Free/Libre
Open-Source Software*).
:::

Sin embargo, existe una diferencia filosófica en su
definición que tiene repercusiones económicas, políticas y
sociales. Para el movimiento comunitario del [*software*
libre] la libertad de los usuarios respecto al *software* es
un imperativo ético. Mientras tanto, la iniciativa del
[código abierto] tiene el objetivo de [atraer a las
empresas] al [*software* libre], por lo que su énfasis está
en su sentido práctico ---disponibilidad del [código
fuente]--- y no en la libertad ---sustento a un ecosistema
de tecnologías libres---.

Las licencias de [*software* libre] y de [código abierto]
suelen ser las mismas. No obstante, el énfasis comercial de
la iniciativa del [código abierto] provoca que las empresas
mezclen el [código fuente] con *software* no libre, en
desmedro de la libertad de los usuarios.

![En este diagrama de Venn se puede apreciar que el
[*software* libre], el [código abierto] y el [*freeware*]
tienen en común la disponibilidad del [código máquina]. Sin
embargo, solo el [*software* libre] y el [código abierto]
comparten la característica de tener disponible el [código
fuente] del programa, lo que permite su estudio y
modificación.][2]

Un ejemplo permite aclarar esta diferencia. Android es quizá
el programa de [código abierto] más popular en la
actualidad. La disponibilidad de su [código fuente] permite
que los operadores de telecomunicaciones o los fabricantes
de teléfonos ofrezcan sistemas operativos Android con
modificaciones. Este es el motivo por el que los teléfonos
Android comparten muchas características entre sí al mismo
tiempo que tienen sus especificidades como las *apps*
preinstaladas o la estética del sistema.

La gran mayoría de estos operadores o fabricantes no tienen
disponible el [código fuente] de las modificaciones que le
han hecho al sistema base de Android porque su licencia le
permite reservarlo para distribución propietaria. Aunque
esto se justifica como modelo de negocio o competitividad
empresarial, la consecuencia es que vuelve muy difícil la
auditoría de estos sistemas. Esta falta de certeza provoca
la sospecha del daño potencial que se podría ocasionar al
usuario de Android, principalmente en lo que se refiere al
monitoreo de su actividad.

Para poder garantizar que el [*software* libre] continúe
siéndolo después de ser modificado, el movimiento del
[*software* libre] usa un mecanismo llamado [*copyleft*],
que implica la herencia de la licencia en cualquier obra
derivada. Este mecanismo se usa para contrarrestar estas
consecuencias que atentan al ecosistema de tecnologías
libres. Aunque en un primer momento es paradójico ---el
[*copyleft*] no te da la libertad de publicar tus programas
derivados con la licencia que te venga en gana---, la lógica
es la siguiente: para garantizar la libertad de todos los
usuarios de *software*, y así fomentar un ecosistema, es
necesario asegurar que las libertades no sean removidas en
su reutilización; por lo tanto, las licencias de [*software*
libre] con [*copyleft*] exigen que sus programas derivados
sean publicados con la misma licencia.

En la actualidad existen una gran cantidad de licencias con
[*copyleft*]. Algunas están diseñadas para el *software*,
otras para base de datos u obras artísticas. [Zettlr] está
licenciado con [[GPL]]{.acronimo} versión 3, mientras que
[Pandoc] y [Scribus] se encuentran licenciados con
[[GPL]]{.acronimo} versión 2, ambas versiones son licencias
*copyleft* y los tres programas que se utilizan en este
tutorial son [*software* libre].

::: {.note .centrado}
Visita [esta lista] para conocer varias de las licencias
[*copyleft*] disponibles.
:::

::: note
El [código abierto] y el [*software* libre] son distintos,
cuentan con [principios] y [definiciones] diferentes que, en
la mayoría de los casos, comparten las mismas licencias. El
movimiento del [*software* libre] considera que el [código
abierto] cuenta con «[valores diferentes]» desde un punto de
vista ético. Para este movimiento es importante usar la
expresión [*software* libre] para enfatizar las
implicaciones en las libertades del usuario.
:::

::: espacio-arriba
La tercera manera de comprender al [*software* libre] es en
contraposición al [*software* propietario].
:::

El [*software* propietario] son los programas que no tienen
disponible su [código fuente] y que prohíben cualquier
modificación o redistribución de sus ejecutables. Para la
comercialización del [código máquina] al usuario se le
ofrece:

-   pago único como Affinity Publisher;
-   modelo de suscripción como Adobe InDesign, o
-   modelo [*freemium*] como Canva ---el usuario tiene
    acceso gratuito pero limitado al menos que pague por ser
    *premium*---.

Aunque la distinción es funcional, un matiz es necesario.
Una gran cantidad de [*software* propietario] depende del
[*software* libre] para su ejecución. Esto se debe a que los
programas de [*software* libre] en varias ocasiones son
usados como [bibliotecas] para los programas propietarios.
Con esto se busca la reducción en la carga de trabajo al
delegar ciertas funciones del programa a sus [bibliotecas].
Por ello, la contraposición entre [*software* propietario] y
[*software* libre] tiene sentido desde el punto de vista del
usuario, pero se matiza desde la perspectiva del
desarrollador que usa las [bibliotecas] para su programa
propietario.

::: note
El [*software* propietario] y el [*software* libre] son
distintos. El [*software* propietario] no tiene disponible
su [código fuente] y prohíbe la modificación o distribución
de sus ejecutables. El [*software* libre] hace público su
[código fuente] y permite cualquier modificación o
distribución.
:::

::: espacio-arriba
Las tres maneras de comprender al [*software* libre] en
comparación al [*freeware*], al [código abierto] y al
[*software* propietario] nos permiten ahora tener más
claridad sobre la comprensión de las 4 libertades que el
[*software* libre] busca garantizar:
:::

1.  Libertad de uso
2.  Libertad de estudio
3.  Libertad de distribución
4.  Libertad de mejora

Estas 4 libertades es lo que garantiza que un programa sea
[*software* libre] ya que para su ejercicio pleno se
requiere del acceso a su [código fuente] y a las licencias
que permitan la modificación o distribución del programa,
unas normas distintas al [*freeware*] y al [*software*
propietario]. El énfasis ético en torno a las libertades del
usuario sobre el [código fuente] es lo que distingue al
[*software* libre] del [código abierto].

De manera personal prefiero simplificar las 4 libertades a
la primera. Por un lado, el estudio, distribución y mejora
de un programa son ejercicios que se dan en *el uso* del
*software*. Por el otro, esto permite hacer patente que la
noción de «libertad» en el *software* es muy concreta: la
libertad es en relación con *el uso* del *software*. Esto
permite aclarar por qué, aunque la [filosofía del *software*
libre] aspira a un entendimiento de la libertad como
«sociedades libres», en la práctica y en lo legal la
libertad queda delimitada a *los usos* del *software*. Quizá
para evitar mayores ambigüedades en la intelección de la
libertad en el contexto del *software* sea más puntual
hablar de «*software* libre de uso» o «*software* de uso
libre» que de «[*software* libre]».

::: {.note .centrado}
Un programa se considera [*software* libre] si su [código
fuente] está disponible y garantiza las libertades de uso,
estudio, distribución y mejora.
:::

Para que un programa sea considerado [*software* libre] ---o
para que un producto cultural sea libre--- se requiere la
adición de una licencia que garantice su uso según las 4
libertades. Esta licencia no se opone al derecho de autor,
sino que lo flexibiliza. En un ejercicio ordinario del
[derecho de autor] la persona autora tiene todos los
derechos reservados sobre su obra. Cuando esta publica con
una licencia libre **no pierde** los derechos sobre su obra,
sino que se reserva algunos derechos ---por lo general son
el derecho de atribución y otros [derechos morales]---.

Los demás derechos, como el de uso, copia, modificación o
distribución son permitidos para cualquier persona. Este
traslado de «todos los derechos reservados» a «algunos
derechos reservados» permite que el usuario pueda usar
libremente una obra sin que peligre el patrimonio del autor.

En el movimiento de la [cultura libre] la persona autora aún
tiene capacidad de decidir el destino de su obra, así como
puede tener mecanismos de retribución. Pensar que el
licenciamiento libre hace que el autor regale su trabajo es
un mito recurrente entre las personas o las entidades que
ven en los ideales de la [cultura libre] una amenaza a su
sustento de vida o a su modelo de negocio.

Para la producción de libros con *software* estas libertades
fomentan un ecosistema con tecnologías disponibles sin
necesidad de un pago y que pueden ser amoldadas según los
intereses u objetivos de un proyecto editorial. Esto quiere
decir que en la edición con [*software* libre] se tiene la
oportunidad de elegir cómo ejecutar un proyecto editorial,
en lugar de delimitarlo a las posibilidades que nos ofrece
algún programa o compañía.

Una ventaja adicional en el uso de herramientas libres es
que estas tienden a consumir menos recursos. Por lo general
un programa libre tiene un tamaño de archivo menor y
necesita menos memoria [RAM]{.acronimo} o [CPU]{.acronimo}
para trabajar. Gracias a esto no hay necesidad de adquirir
equipo de cómputo o de contar con una máquina potente para
hacer una publicación con calidad editorial.

![El [*software* libre] busca fomentar un ecosistema en
donde los usuarios usan y comparten libremente los frutos de
su trabajo.][3]

Uno de los prejuicios sobre el [*software* libre] es la
suposición de que sus programas son de menor calidad en
relación con los programas propietarios. No obstante, como
se mencionó con anterioridad, la gran mayoría del
[*software* propietario] usa el [*software* libre] como
[biblioteca][bibliotecas] debido a su fiabilidad. La
cuestión tal vez tiene su origen a dos carencias recurrentes
en los programas libres destinados al usuario final:

1.  Una experiencia de usuario poco grata
2.  Un entorno gráfico menos atractivo

No obstante, de manera paulatina el [*software* libre] está
ganando su lugar en el usuario final. Para quienes desean
hacer publicaciones con herramientas libres todavía hay
mucho camino por recorrer, aunque las bases ya están
presentes: flujos de trabajo eficaces, comunidades que dan
soporte incondicional, además de herramientas potentes y de
gran calidad.

  [Zettlr]: https://www.zettlr.com
  [Pandoc]: https://pandoc.org
  [Scribus]: https://www.scribus.net
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [*freeware*]: https://es.wikipedia.org/wiki/Software_gratis
  [código fuente]: https://es.wikipedia.org/wiki/C%C3%B3digo_fuente
  [código máquina]: https://es.wikipedia.org/wiki/Lenguaje_de_m%C3%A1quina
  [1]: assets/img/codigo.jpg
  [*malware*]: https://es.wikipedia.org/wiki/Malware
  [no es requisito]: https://www.gnu.org/philosophy/selling.es.html
  [código abierto]: https://es.wikipedia.org/wiki/C%C3%B3digo_abierto
  [FOSS]: https://es.wikipedia.org/wiki/Software_libre_y_de_c%C3%B3digo_abierto
  [atraer a las empresas]: http://www.catb.org/~esr/open-source.html
  [2]: assets/img/venn.jpg
  [*copyleft*]: https://es.wikipedia.org/wiki/Copyleft
  [GPL]: https://es.wikipedia.org/wiki/GNU_General_Public_License
  [esta lista]: https://www.gnu.org/licenses/license-list.html
  [principios]: https://opensource.org/osd
  [definiciones]: https://www.gnu.org/philosophy/free-sw.es.html
  [valores diferentes]: https://www.gnu.org/philosophy/open-source-misses-the-point.es.html
  [*software* propietario]: https://es.wikipedia.org/wiki/Software_propietario
  [*freemium*]: https://es.wikipedia.org/wiki/Freemium
  [bibliotecas]: https://es.wikipedia.org/wiki/Biblioteca_(inform%C3%A1tica)
  [filosofía del *software* libre]: https://www.gnu.org/philosophy/philosophy.es.html
  [derecho de autor]: https://es.wikipedia.org/wiki/Derecho_de_autor
  [derechos morales]: https://es.wikipedia.org/wiki/Derechos_morales
  [cultura libre]: https://es.wikipedia.org/wiki/Cultura_libre
  [3]: assets/img/foss.jpg
